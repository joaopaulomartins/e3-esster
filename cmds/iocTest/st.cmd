require esster

# set PREFIX
epicsEnvSet("PREFIX", "TEST:LEADER:")

## Load ESSTER GENERATOR records
dbLoadRecords("$(esster_DB)/essTimeRefGenerator.template", "P=$(PREFIX), R=")

## Load tester records
epicsEnvSet("PREFIX_INT", "TEST:LOCAL:")
dbLoadRecords("$(E3_CMD_TOP)/essterIocTest-setpoints.template", "P=$(PREFIX), R=, ESSTER=$(PREFIX), TIMESTAMP_REC=$(PREFIX)ExampleTimeSource")
dbLoadRecords("$(E3_CMD_TOP)/essterIocTest.template", "P=$(PREFIX_INT), R=, ESSTER=$(PREFIX)")

iocInit()

