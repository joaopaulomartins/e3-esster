require esster

#- Set leader and follower prefix
epicsEnvSet("PREFIX", "TEST:FOLLOWER:")
epicsEnvSet("LEADER", "TEST:LEADER:")

epicsEnvSet("UTAG_SOURCE", "$(LEADER)UTAGRef")
epicsEnvSet("TIME_SOURCE", "$(LEADER)TimeRef")
dbLoadRecords("$(esster_DB)/essTimeRefReceiver.template", "P=$(PREFIX), R=, TIME_SOURCE=$(TIME_SOURCE), UTAG_SOURCE=$(UTAG_SOURCE)")

epicsEnvSet("TSEL_REF", "$(PREFIX)TimeRef")
dbLoadRecords("$(E3_CMD_TOP)/esster-extclient.template", "P=$(PREFIX), R=, TSEL_REF=$(TSEL_REF)")

iocInit

